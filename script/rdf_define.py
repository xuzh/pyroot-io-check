import ROOT

# initialize a RDataFrame with 100 events
rdf = ROOT.RDataFrame(100)

# 1) same number for all events
rdf = rdf.Define('b1', '1')

# 2) ROOT function, with C/C++ language
rdf = rdf.Define('b2', 'gRandom->Rndm();')

# 3) function of other branch (column)
rdf = rdf.Define('b3', 'b1 + b2')

# 4) complex function with C/C++
rdf = rdf.Define('b4', ''' Int_t a = 2;
                           Int_t b =-2;
                           Double_t output;
                           if ( b2 <= 0.5 ) 
                           {
                               output = a;
                           }else
                           {
                               output = b;
                           }
                           return output;
                       ''')

# TO BE TEST
# def func(x, y):
#     return x*y

# # 5) python function, with additional parameter (list of input variables).
# rdf = rdf.Define('b5', func, ['b1', 'b2'])

# 6) redefine
rdf = rdf.Define('b6', 'b1')
rdf = rdf.Redefine('b6', 'b1+1')

print(rdf.Describe())
print(rdf.Display().Print())
