
import ROOT
import uproot
import awkward as ak
import time

file_path = '../root/pip_p2.00theta9.00.root'
tree_name = 'Cerenkov_vector'

def open_with_ROOT():
    E_data = []
    X_data = []
    Y_data = []
    Z_data = []
    
    with ROOT.TFile.Open(f'{file_path}') as f:
        tree = f.Get(tree_name)
        for event in tree:
            E_data.append(list(event.CheE))
            X_data.append(list(event.CheX))
            Y_data.append(list(event.CheY))
            Z_data.append(list(event.CheZ))
    
    # Flaten
    E_data_out = [i for sublist in E_data for i in sublist]
    X_data_out = [i for sublist in X_data for i in sublist]
    Y_data_out = [i for sublist in Y_data for i in sublist]
    Z_data_out = [i for sublist in Z_data for i in sublist]
    
def open_with_uproot():
    with uproot.open(f'{file_path}:{tree_name}') as t:
        data = t.arrays(['CheE', 'CheX', 'CheY', 'CheZ'])
    
    # Flaten
    E_data_out = ak.flatten(data['CheE']).tolist()
    X_data_out = ak.flatten(data['CheX']).tolist()
    Y_data_out = ak.flatten(data['CheY']).tolist()
    Z_data_out = ak.flatten(data['CheZ']).tolist()
    
if __name__ == '__main__':
    t0 = time.perf_counter()
    open_with_ROOT()
    t1 = time.perf_counter()
    open_with_uproot()
    t2 = time.perf_counter()
    
    print( f'ROOT uses {t1 - t0}' )
    print( f'uproot uses {t2 - t1}' )
    